
 require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
  })



module.exports = {
  siteMetadata: {
    title: "tolka playground",
    titleTemplate: "%s · jsut some fun",
    description:"Tolka web development.",
    url: "https://fun.tolka.io", // No trailing slash allowed!
    image: "/images/snape.jpg", // Path to your image you placed in the 'static' folder
    twitterUsername: "@tolkadot",

  },
 
  plugins: [
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        /*
         */
                // This type will contain remote schema Query type
        typeName: `WPGraphQL`,
        // This is field under which it's accessible
        fieldName: `wpgraphql`,
        // Url to query from
        url: `https://wordpress-234168-1214751.cloudwaysapps.com/graphql`,
      },
    },
    {
      resolve: `gatsby-plugin-theme-ui`,
      options: {
        preset: "@theme-ui/tailwind",
      },
    },
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sass",
    "gatsby-plugin-image",  
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: `gatsby-source-formium`,
      options: {
        projectId: process.env.GATSBY_FORMIUM_PROJECTID,
        accessToken: process.env.FORMIUM_TOKEN,
      },
    },
    
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `audio-C0re`,
        short_name: `audio-C0re`,
        start_url: `/`,
        background_color: `#f7f0eb`,
        theme_color: `#a2466c`,
        display: `standalone`,
        icon: `src/images/audio-C0re.png`, // This path is relative to the root of the site.
        icons: [
          {
            src: `/favicons/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/favicons/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ], 
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [`/contact/`, `/location/*`],
      },
    },
    {
    resolve: "gatsby-plugin-react-svg",
    options: {
      rule: {
        include: /images/ // 
      }
    }
  }

  ],
};
