exports.createPages = async ({ actions, graphql, reporter }) => {
    const result = await graphql(`
      {
        allWpPost {
          nodes {
            id
            slug
          }
        }
        allWpCity {
            nodes {
              id
              slug
            }
          }
      }
    `)
  
    if (result.errors) {
      reporter.error("There was an error fetching posts", result.errors)
    }

    const BlogPosts = result.data.allWpPost;
    const CityPost = result.data.allWpCity;
  
    // Define the template to use
    const templateBlog = require.resolve(`./src/templates/blog.js`)
    const templateCity = require.resolve(`./src/templates/city.js`)

  
    if (BlogPosts.nodes.length) {
        BlogPosts.nodes.map(post => {
        actions.createPage({
          path:`/news/${post.slug}`,
          component: templateBlog,
          context: post,
        })
      })
    }

    if (CityPost.nodes.length) {
        CityPost.nodes.map(post => {
        actions.createPage({
          path:`/city/${post.slug}`,
          component: templateCity,
          context: post,
        })
      })
    }

  }