export function formatDateString(dateString, locale) {
    const date = new Date(dateString);
  
    return date.toLocaleDateString(locale, {
      month: 'long',
      year: 'numeric',
      day: 'numeric',
    });
  }
  