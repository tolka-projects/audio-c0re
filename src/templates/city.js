import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"


const WpCity = ({location,
    data: {
      wpCity: { title, content, id },
    },
  }) => {
    return (
      <Layout>
        <Seo location={location} />
        <div className={`City-${id}`}>
          <h1>{title}</h1>
          This is the /templates/city.js template
          <div dangerouslySetInnerHTML={{ __html: content }} />
        </div>
      </Layout>
    )
  }

  export const query = graphql`
  query($id: String) {
    wpCity(id: { eq: $id }) {
      id
      title
      content
    }
  }
`

export default WpCity
