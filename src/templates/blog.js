import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"


// import { StaticImage } from "gatsby-plugin-image"

const WpPost = ({
    data: {
      wpPost: { title, content, id },
    },
  }) => {
    return (
      <Layout>
      <div className="container mt-header pt-5">
        <div className={`post-${id}`}>
          <h1>{title}</h1>
          This is the /templates/blog.js template

          <div dangerouslySetInnerHTML={{ __html: content }} />
        </div>
      </div>
      </Layout>
    )
  }

  export const query = graphql`
  query($id: String) {
    wpPost(id: { eq: $id }) {
      id
      title
      content
    }
  }
`

export default WpPost
