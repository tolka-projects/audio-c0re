
import React, { useState, useEffect} from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { StaticImage } from "gatsby-plugin-image"
import './footer.scss';


// import NavDesktop from "../components/nav-desktop"
// import NavMobile from "../components/nav-mobile"
// import Navigation from "../components/navigation"

//import '../components/scss/header.scss';


export default function Footer({}) {


  return (
  <StaticQuery
    query={graphql`
      query {
        wpMenu(id: {eq: "dGVybTo1MA=="}) {
          menuItems {
            nodes {
              databaseId
              title
              url
              label
            }
          }
        }
        wp{
          myOptionsPage {
            footer {
              email
              fieldGroupName
              mailingAddress
              officeAddress
              phoneData
              phoneDisplay
            }
          }
        } 
      }
    `}
  render={data => (
   <footer className='footer-main container--full-width bg-color--secondary py-3'>
    <div className="container  grid--three">
      <div>
        <div>{data.wp.myOptionsPage.footer.email}</div>
      </div>
      <div className="footer-main--logo position-relative">
          <a href="/" className="custom-logo-link" rel="home" title="">
            <StaticImage 
              src="../../images/lightning-bolt.svg" 
              alt="A building"
              placeholder="blurred"
              layout="fixed"
              width={100}
              height={100} />
          </a>
      </div>

      <nav className="footer-navigation--nav">
        <ul className="footer-navigation--menu list-unstyled" >
        {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId }) => (
          <li key={databaseId} className="list-unstyled">
            <a title={title} href={url}>
              {label}
            </a>
          </li>
        ))}
        </ul> 
      </nav>

      </div>
  </footer>
)} 
/>
  )}
