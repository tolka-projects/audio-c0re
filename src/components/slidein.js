import React, { useState, useEffect} from 'react';
//import ReactDOM from 'react-dom';
//import { Link } from 'gatsby';
//import '../components/scss/Slidein.scss';

function SlideIn(props) {

    const [slideInOpen, setslideInOpen] = useState(false);
    function openSlideIn()  {
        setslideInOpen(true);
        sessionStorage.setItem('slide-status', 1);
    }

    function closeSlideIn()  {
        setslideInOpen(false);
    }
        
    useEffect(() => {
        let slideStatus = sessionStorage.getItem('slide-status');
        if(!slideStatus){
            const timeId = setTimeout(() => openSlideIn(), 2000);
            return () => clearTimeout(timeId)
        }
    }, []);

    return (
     <div className={`slidein-container ${slideInOpen ? "is-visible" : ""}`} id="slidein-container"  style={{ visibility: slideInOpen ? "visible" : "hidden" }}>
        <div className='slidein-content'>
       
           <div className="slidein-close">
                <button className="slidein-close" id="slidein-close" onClick={() => closeSlideIn()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="13.246" height="13.246" viewBox="0 0 13.246 13.246">
                        <g id="Group_345" data-name="Group 345" transform="translate(-1101.146 -429.783)">
                        <line id="Line_36" data-name="Line 36" y1="12.539" x2="12.539" transform="translate(1101.5 430.137)" fill="none" stroke="#ffffff" stroke-width="1"/>
                        <line id="Line_37" data-name="Line 37" x1="12.539" y1="12.539" transform="translate(1101.5 430.137)" fill="none" stroke="#ffffff" stroke-width="1"/>
                        </g>
                    </svg>
                </button>
            </div>  
            <div>
                <h2 className="">Book Now for Spring/Summer</h2>
                <p className="">Start the process now to have your product done for Spring/Summer 2021. </p>
                <p className="">Contact us today and we can ensure the supply is ready to kick off your project with no delays.</p>
                <div>
                    <a className="" href="/contact/"> Pencil Me In!</a>
                </div>
            </div>   

        </div>
     </div>
    );
  }


export default SlideIn
