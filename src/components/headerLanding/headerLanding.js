
import React, { useState, useEffect} from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { StaticImage } from "gatsby-plugin-image"

// import NavDesktop from "../components/nav-desktop"
// import NavMobile from "../components/nav-mobile"
// import Navigation from "../components/navigation"
//import '../components/scss/header.scss';


const HeaderLanding = ({ }) => {
  
  const [navOpen, setnavOpen] = useState(false);
  //desktop drowdown
  const [dd1Shown, setdd1Shown] = useState(false);
  //mobile dropdown
  const [dd1Open, setdd1Open] = useState(false);


  function opennav()  {
    //console.log("open")
        setnavOpen(true);
    }

  function closenav()  {
     //console.log("close")
      setnavOpen(false);
  }

   function opendd1()  {
    //console.log("open")
        setdd1Open(true);
    }

  function closedd1()  {
     //console.log("close")
      setdd1Open(false);
  }
        
  useEffect(() => {
        
  }, []);



  return (
  <StaticQuery
    query={graphql`
      query {
        wpMenu(id: {eq: "dGVybTo1MA=="}) {
          menuItems {
            nodes {
              databaseId
              title
              url
              label
              cssClasses
            }
          }
        }
        wp{
          myOptionsPage {
            pageSlug
            pageTitle
            header {
              subnavigation {
                subNavItem {
                  target
                  title
                  url
                }
              }
            }
          }
        }  
      }
    `}
  render={data => (
   <header className='landing--header-main mx-10 my-6' >
      <nav className="landing--header-navigation--nav d-flex justify-content-end">
        <h2 id="main-nav-label" className="visually-hidden">Main Navigation</h2>
        <a className="visually-hidden-focusable" href="#content">Skip to main content</a>


        <div className="d-flex align-items-center">
          <button onClick={navOpen ? closenav : opennav } id="landing--header-navigation--toggle" className= {navOpen ? 'landing--header-navigation--toggle' : 'landing--header-navigation--toggle closed'} type="button" data-toggle="collapse" data-target="#popover-navigation--menu" aria-controls="#popover-navigation--menu" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>

        <div className={navOpen ? 'landing--popover-navigation--menu active' : 'landing--popover-navigation--menu'} id="landing--popover-navigation--menu">
          <div className="landing--header-navigation--container">

            <ul className="landing--header-navigation--menu" >
            {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId, cssClasses }) => (
            
              <li key={databaseId} className={cssClasses[0]}>

                <a title={title} href={url}>
                  {label} 
                </a>

                
              </li>

            ))}

            </ul> 
          </div>
        </div>
         
      </nav>
  </header>
)} 
/>
  )}

export default HeaderLanding