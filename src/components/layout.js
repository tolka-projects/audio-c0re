import React from 'react';
import Header from "../components/header/header"
import Footer from "./footer/footer"


import  SlideIn from '../components/slidein';
import "../sass/theme.scss"

export default function Layout({ children }) {
  return (
    <div>
      <Header />
        <main>
          <div class="container py-8">
          {children}
          </div>
        </main>
       <Footer />
      <SlideIn />
    </div>
  )
}