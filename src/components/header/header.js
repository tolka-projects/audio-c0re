
import React, { useState, useEffect} from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { StaticImage } from "gatsby-plugin-image"

// import NavDesktop from "../components/nav-desktop"
// import NavMobile from "../components/nav-mobile"
// import Navigation from "../components/navigation"

//import '../components/scss/header.scss';


const Header = ({ }) => {
  
  const [navOpen, setnavOpen] = useState(false);
  //desktop drowdown
  const [dd1Shown, setdd1Shown] = useState(false);
  //mobile dropdown
  const [dd1Open, setdd1Open] = useState(false);


  function opennav()  {
    //console.log("open")
        setnavOpen(true);
    }

  function closenav()  {
     //console.log("close")
      setnavOpen(false);
  }

   function opendd1()  {
    //console.log("open")
        setdd1Open(true);
    }

  function closedd1()  {
     //console.log("close")
      setdd1Open(false);
  }
        
  useEffect(() => {
        
  }, []);



  return (
  <StaticQuery
    query={graphql`
      query {
        wpMenu(id: {eq: "dGVybTo1MA=="}) {
          menuItems {
            nodes {
              databaseId
              title
              url
              label
              cssClasses
            }
          }
        }
        wp{
          myOptionsPage {
            pageSlug
            pageTitle
            header {
              subnavigation {
                subNavItem {
                  target
                  title
                  url
                }
              }
            }
          }
        }  
      }
    `}
  render={data => (
   <header className='header-main'>
    <div className="container">
      <nav className="header-navigation--nav">
        <h2 id="main-nav-label" className="visually-hidden">Main Navigation</h2>
        <a className="visually-hidden-focusable" href="#content">Skip to main content</a>

        <div className="header-navigation--logo position-relative">
            <a href="/" className="custom-logo-link" rel="home" title="">
              <StaticImage 
                src="../../images/lightning-bolt.svg" 
                alt="A building"
                placeholder="blurred"
                layout="fixed"
                width={50}
                height={50} />
            </a>
        </div>


        <div className="d-flex align-items-center">
          <button onClick={navOpen ? closenav : opennav } id="header-navigation--toggle" className="header-navigation--toggle closed" type="button" data-toggle="collapse" data-target="#popover-navigation--menu" aria-controls="#popover-navigation--menu" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>

        <div className={navOpen ? 'popover-navigation--menu active' : 'popover-navigation--menu'} id="popover-navigation--menu">
          <div className="header-navigation--container">

            <ul className="header-navigation--menu" >
            {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId, cssClasses }) => (
            
              <li key={databaseId} className={cssClasses[0]}>

                <a title={title} href={url}>
                  {label} 
                </a>

              {cssClasses.includes('dropdown-1') && (
                <button className="visually-hidden-desktop popover-navigation--dropdown-toggle no-styles" onClick={dd1Open ? closedd1 : opendd1 }>
                <svg width="21px" height="21px" viewBox="0 0 21 21" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Group-4-Copy-15" stroke="#ffffff">
                            <line x1="-1.13659082e-13" y1="10.5" x2="21" y2="10.5" id="Line-11-Copy-6"></line>
                            <line x1="10.5" y1="0" x2="10.5" y2="21" id="Line-11-Copy-6"></line>
                        </g>
                    </g>
                </svg>
                <span className="visually-hidden">show submenu for Home </span>
                </button>
                )}

                {cssClasses.includes('dropdown-1') && (
                  <div className={dd1Open ? 'sub-nav--one active' : 'sub-nav--one'}>
                    <ul>
                      {data.wp.myOptionsPage.header.subnavigation.map(({ subNavItem }) => (
                        <li key={subNavItem.title}>
                          <a title={subNavItem.title} href={subNavItem.url}>
                            {subNavItem.title}
                          </a>
                        </li>
                      ))}
                    </ul> 
                  </div>
                )}
                
              </li>

            ))}

            </ul> 
          </div>
        </div>

         {dd1Shown && (
          <div className='sub-nav--one'>
              <ul>
                {data.wp.myOptionsPage.header.subnavigation.map(({ subNavItem }) => (
                  <li key={subNavItem.title}>
                    <a title={subNavItem.title} href={subNavItem.url}>
                      {subNavItem.title}
                    </a>
                  </li>
                ))}
              </ul> 
            </div>
         )}
         

      </nav>
      </div>
  </header>
)} 
/>
  )}

export default Header