
//import { jsx, Flex, Link, NavLink, Box, MenuButton} from "theme-ui"
// import React from "react"
import { StaticQuery, graphql } from 'gatsby';
import React, { useState, useEffect} from 'react';


export default function NavMobile() {
  
  const [navOpen, setnavOpen] = useState(false);

  function opennav()  {
        setnavOpen(true);
    }

  function closenav()  {
      setnavOpen(false);
  }

        
    useEffect(() => {
       
          
        }, []);

  return (
    <StaticQuery
      query={graphql`
        query {
          wpMenu(id: {eq: "dGVybTo1MA=="}) {
            menuItems {
              nodes {
                databaseId
                title
                url
                label
              }
            }
          }
        }
      `}
      render={data => (
      <div>
        <nav className="header--nav--desktop">
          <button aria-label="Toggle Menu" onClick={navOpen ? closenav : opennav }>toggle menu</button>    
          <ul style={{visibility: navOpen ? "visible" : "hidden" }}>
            {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId }) => (
              <li key={databaseId} sx={{listStyleType: 'none' }}>
                <a title={title} href={url}>
                  {label}
                </a>
              </li>
            ))}
          </ul> 
          </nav>
        </div>
      )}
    />
  )
}