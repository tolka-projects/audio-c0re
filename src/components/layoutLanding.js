import React from 'react';
import HeaderLanding from "../components/headerLanding/headerLanding"
import  SlideIn from '../components/slidein';
import "../sass/theme.scss"

export default function LayoutLanding({ children }) {
  return (
    <div>
    <HeaderLanding />
        <main>
          {children}
        </main>
    <SlideIn />    
    </div>
  )
}