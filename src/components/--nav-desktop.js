
import React from "react"
import { StaticQuery, graphql } from 'gatsby';
//import React, { useState, useEffect} from 'react';


export default function NavDesktop() {
  
  //const [navOpen, setnavOpen] = useState(false);

  return (
    <StaticQuery
      query={graphql`
        query {
          wpMenu(id: {eq: "dGVybTo1MA=="}) {
            menuItems {
              nodes {
                databaseId
                title
                url
                label
              }
            }
          }
        }
      `}
      render={data => (
       <nav className="header--nav--desktop" style={{position: 'relative', zIndex: '9' }}>
            {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId }) => (
              <li key={databaseId} sx={{listStyleType: 'none' }}>
                <a  title={title} href={url}>
                  {label}
                </a>
              </li>
            ))}
        </nav>
      )}
    />
  )
}