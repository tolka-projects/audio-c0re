
//import { jsx, Flex, Link, NavLink, Box, MenuButton} from "theme-ui"
// import React from "react"
import React, { useState, useEffect} from 'react';
import { StaticQuery, graphql } from 'gatsby';


export default function Navigation() {
  
  const [navOpen, setnavOpen] = useState(false);

  function opennav()  {
        setnavOpen(true);
    }

  function closenav()  {
      setnavOpen(false);
  }

        
    useEffect(() => {
           
        }, []);

  return (
    <StaticQuery
      query={graphql`
        query {
          wpMenu(id: {eq: "dGVybTo1MA=="}) {
            menuItems {
              nodes {
                databaseId
                title
                url
                label
              }
            }
          }
          wp{
            myOptionsPage {
              header {
               subnavigation {
                subNavItem {
                  target
                  title
                  url
                  }
                }
              }
            } 
          }
        }
      `}
      render={data => (
      <div class="popover-navigation--menu " id="popover-navigation--menu">
			    <div class="header-navigation--container">

            <button aria-label="Toggle Menu" onClick={navOpen ? closenav : opennav }>toggle menu</button>    
            <ul>
              {data.wpMenu.menuItems.nodes.map(({ label, title, url, databaseId }) => (
                <li key={databaseId}>
                  <a title={title} href={url}>
                    {label}
                  </a>
                </li>
              ))}
            </ul> 
            <ul>
              {data.myOptionsPage.header.subnavigation.subNavItem.map(({ title, url }) => (
                <li key={title}>
                  <a title={title} href={url}>
                    {title}
                  </a>
                </li>
              ))}
            </ul> 
        </div>
      </div>
      )}
    />
  )
}



// wpMenu(id: {eq: "dGVybTo1MA=="}) {
//     menuItems {
//       nodes {
//         databaseId
//         url
//         label
//         childItems {
//           nodes {
//             id
//             url
//             label
//             databaseId
//           }
//         }
//       }
//     }
//   }