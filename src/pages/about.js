import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from '../components/seo';
import Header from "../components/header/header"

import { convertToBgImage } from "gbimage-bridge"
import BackgroundImage from 'gatsby-background-image'
import {getImage } from "gatsby-plugin-image"



const AboutPage = ({data , location}) => {
//console.log(data);
const image = getImage(data.wpPage.landingPage.heroImage.localFile)
const bgImage = convertToBgImage(image)

//console.log(image, "image");

return(
<div class="">
  <Seo location={location} />
  <Header />

<BackgroundImage
      Tag="section" className="container--full-width container--hero"
      // Spread bgImage into BackgroundImage:
      {...bgImage}
      preserveStackingContext
    >
      
  <div className="container--hero-opacity"></div>
    <div className="container--medium text-center py-8">
        <p className="color--white mb-0 text-uppercase">{data.wpPage.landingPage.subTitle}</p>
        <h1 className="color--white">{data.wpPage.landingPage.title}</h1>

        <div className="color--white" dangerouslySetInnerHTML={{ __html: data.wpPage.landingPage.introText }} ></div>
        <div className="d-flex justify-content-center">
            {data.wpPage.landingPage.ctaButton.map(node=> {
            //console.log(node.ctaButton.target);
              if(node.ctaButton.target ==='_blank') {
                  return(
                      <div className="pe-3">
                          <a className="button" href={node.ctaButton.url} target='_blank' rel="noreferrer">{node.ctaButton.title}</a>
                      </div>
                  )      
              } else { 
                  return (
                      <div>
                          <a className="button--reverse" href={node.ctaButton.url}>{node.ctaButton.title}</a>
                      </div>
                  )
              }
            })}
    </div>
  </div>
  
  </BackgroundImage>
  <section className="container text-center py-5 py-md-8">
    <div class="container">
      <p>{data.wpPage.landingPage.introSection.heading}</p>
      <p>{data.wpPage.landingPage.introSection.introText}</p>
    </div>
  </section>
</div>

  )
}


export const query = graphql`
  {
    wpPage(databaseId: {eq: 21}) {
      id
      title
      landingPage {
        introText
        subTitle
        title
        heroImage {
          localFile {
            childImageSharp {
              gatsbyImageData
            }
          }
          altText
        }
        backgroundImage {
          localFile {
            childImageSharp {
              gatsbyImageData
            }
          }
        }
        ctaButton {
          ctaButton {
            target
            title
            url
          }
        }
        introSection {
          heading
          introParagraph
          icon {
            localFile {
              publicURL
            }
            altText
          }
        }
      }
    }
  }
`


export default AboutPage