import React from "react"
import { graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { formatDateString } from '../../lib/utils';
import Layout from "../../components/layout"
import Seo from "../../components/seo"

//import FeaturedMedia from "../../components/FeaturedImage"



// export default function News({ data }) {

// }


const News = ({ location, 

  data: {
    wpPost: {
      title,
      content,
      dateGmt,
      featuredImage,
    },
  },

 }) => {

  const image = getImage(featuredImage.node.localFile)
  ////console.log(image);

  return(
    <Layout>
    <Seo location={location} />
      <div>  
          <div>
              <p>Title : {title}</p>
              <p>Publish Date : {formatDateString(dateGmt)}</p>
              <p>A static image : </p>
              <StaticImage src="../../images/building-city.svg" alt="A dinosaur" />
              <p>The Wordpress Featured  image : </p>

              <GatsbyImage image={image}  alt=""/>

              <h2>The Wordpress Post Content</h2>
              <div  dangerouslySetInnerHTML={{ __html: content }} ></div>
          </div>
      </div>
    </Layout>
  )
}


export const query = graphql`
  query blogPostQuery($id: String!) {
    wpPost(id: { eq: $id }) {
      title
      content
      dateGmt
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              gatsbyImageData(layout: CONSTRAINED, quality: 90)
            }
            publicURL
          }
          mediaDetails {
            width
            height
          }
        }
      }
    }
  }
`;

export default News