/* @jsx jsx */
//import React from "react"
import { jsx, Grid, Flex} from "theme-ui"
import { StaticImage } from "gatsby-plugin-image"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

import { convertToBgImage } from "gbimage-bridge"
import BackgroundImage from 'gatsby-background-image'



const imagesPage = ({data}) => {
//console.log(data);
const image = getImage(data.wpPage.landingPage.heroImage.localFile)
const bgimage = getImage(data.wpPage.landingPage.backgroundImage.localFile)

const bgImageFull = convertToBgImage(bgimage)
//console.log(image)
//console.log(bgimage)
//console.log(bgImageFull)
//console.log(data.wpPage.landingPage.svgImage.localFile.publicURL)


  return(


<Flex>
  <Grid gap={4} columns={[ '1fr' , '1fr', '1fr 1fr']} sx={{}}>

  
    <Flex sx={{
      backgroundImage:'url(/static/audio-core-landing-bg-f60a0bc7324ffcb492153925d7d57a23.svg)',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      flexDirection: 'column',
      border: '1px solid red',}}
    >
      <h2>static background image using svg stored locally </h2>
      <GatsbyImage image={image}  alt=""/>
      <h2>GatsbyImage must be either .png or .jpg. SVG is NOT supported by gatsby-plugin-image </h2>

    </Flex>


    <BackgroundImage
    Tag="section"
      // Spread bgImage into BackgroundImage:
      {...bgImageFull}
      preserveStackingContext
      sx={{border: '1px solid red',}}
    >
      <h2>static background image - on cms - must be .png or jpg; these conditions are enforced by acf </h2>
      <GatsbyImage image={image}  alt=""/>

    </BackgroundImage>


    <Flex style={{backgroundImage: 'url(' + data.wpPage.landingPage.svgImage.localFile.publicURL + ')' }} sx={{
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      flexDirection: 'column',
      border: '1px solid red',}}
    >
      <h2>svg stored on cms - must be svg  - these conditions are enforced by acf </h2>
      <GatsbyImage image={image}  alt=""/>

    </Flex>
    
    <Flex>    
    <h2>This is a static image </h2>

      <StaticImage 
        src="../images/building-city.svg" 
        alt="A building"
        placeholder="blurred"
        layout="fixed"
        width={200}
        height={200} />

    </Flex>    

  </Grid>
</Flex>
 
  )
}


export const query = graphql`
  {
    wpPage(databaseId: {eq: 21}) {
      id
      title
      landingPage {
        introText
        subTitle
        title
        heroImage {
          localFile {
            childImageSharp {
              gatsbyImageData
            }
          }
          altText
        }
        backgroundImage {
          id
          localFile {
            childImageSharp {
              gatsbyImageData
            }
          }
        }
        svgImage {
          id
          localFile {
            url
            sourceInstanceName
            publicURL
          }
        }
      }
    }
  }
`


export default imagesPage