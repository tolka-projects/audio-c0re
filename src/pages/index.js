import React, {useEffect} from 'react';
import { graphql } from "gatsby"
import LayoutLanding from "../components/layoutLanding"
import Seo from '../components/seo';
import {getImage } from "gatsby-plugin-image"
import { convertToBgImage } from "gbimage-bridge"
import BackgroundImage from 'gatsby-background-image'
import Arrow from '../images/tolka-arrow-down.svg'
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

// if (typeof window !== `undefined`) {
//  gsap.registerPlugin(ScrollTrigger)
//  gsap.registerPlugin(SplitText)
//  gsap.defaults({ overwrite: "auto", duration: 0.3 })
// }

//import NavStatic from "../components/nav-static"
// import { convertToBgImage } from "gbimage-bridge"
// import BackgroundImage from 'gatsby-background-image'
// import { GatsbyImage, getImage } from "gatsby-plugin-image"

let splitMyString = (str, splitLength) => {
  var a = str.split(' '), b = [];
  while(a.length) b.push(a.splice(0,splitLength).join(' '));
  return b;
}


if (typeof window !== "undefined") {
  gsap.registerPlugin(ScrollTrigger); 
}


const TolkaPage = ({data , location}) => {
//console.log(data);
const image = getImage(data.wpPage.page_tolka.featureImage.localFile)
const bgImageFull = convertToBgImage(image)
//console.log(image);
//const boxRef = useRef();

 useEffect(() => {

// intro animation //
gsap.set(".circle--child-1", { yPercent: -200 });
  const tl = gsap.timeline();
  tl.to('.circle--child-1', {
        // xPercent: -20,
        opacity: 1,
        marginLeft: "0px",
        stagger: .3,
        duration: .5,
        yPercent: "0",
        ease: "power1.inOut",
   })

    tl.to('.box div:first-child', {
        opacity: 1,
        marginLeft: "0px",
        stagger: 0.3,
        duration: .3,
        ease: "power1.inOut",
        delay:.3})


    tl.to('.box div:nth-child(2)', {
        opacity: 1,
         marginLeft: "0px",
        stagger: 0.3,
        duration: .3,
        ease: "power1.inOut", 
        delay:.3 })
   

    tl.to('.box div:nth-child(3)', {
        // xPercent: -20,
        opacity: 1,
         marginLeft: "0px",
        stagger: .3,
        duration: .3,
        //scale: -1,
        ease: "power1.inOut",
        delay:.3 })


// offset scrolling circles animation //
  gsap.to(".circle--child-1", {
    scrollTrigger: {
        trigger: ".circle--child-trigger", // start the animation when ".box" enters the viewport (once)
        toggleActions:"play pause reverse pause",
        start: "top center",
//        markers: true,
        scrub: true
    },
    y: 80,
    duration: 2, 
    ease: "power1.inOut",
    delay: "1"
  });


  gsap.from(".circle--child-2", {
    scrollTrigger: {
        trigger: ".circle--child-trigger", // start the animation when ".box" enters the viewport (once)
        toggleActions:"play pause reverse pause",
        start: "top center",
//        markers: true,
        scrub: true
    },
    y: 100,
    duration: 3, 
    ease: "power1.inOut",
    delay: "1"
  });


// scrolling text animation //
  gsap.to('.scrolling-text--wrapper h2', {
  x: 1000,
  scrollTrigger: {
    trigger: '.scrolling-text--container',
    scrub: 0.5 } 
  });

  gsap.to('.scrolling-text--wrapper p', {
  x: -1000,
  scrollTrigger: {
    trigger: '.scrolling-text--container',
    scrub: 0.5 } 
  });


// arrow animation //
gsap.set(".arrow:first-of-type", { yPercent: -100 });
gsap.to(".arrow", { yPercent: "+=100", repeat: -1, repeatDelay: 2 });





let sections = gsap.utils.toArray(".fire-text--container > div");

sections.forEach(function (element, index) {
  let tl = gsap
    .timeline({
      scrollTrigger: {
        trigger: element,
        start: "top 50%",
        end: "+=250px",
        scrub: true,
        // markers: true
      }
    })
    .to(
      element.querySelector("h2"), {
        backgroundImage: "linear-gradient(#E35874 100%, #ff7f66 200%, #ff7f66 300%)",      
        duration: 2,
        ease: "none",
        opacity: 1,
      }
    )
    .from(
      element.querySelectorAll("p"),
      {
        duration: 2,
        opacity: 0,
        y: 50,
        stagger: 0.2
      },
    );
});


gsap.set(".grow--container", { height: 0});
gsap.to('.grow--container', {
  scrollTrigger: {
    trigger: '.fire-text--container',
    start: "bottom 50%",
    } ,
    height: "100%",
    duration: 2,
    scrub: true,
    //markers: true,
  });


  });





const heroTitle = splitMyString(data.wpPage.page_tolka.introText, 3);
////console.log(heroTitle);
return(

<LayoutLanding>

<Seo location={location} />



  <div className="pb-10 mb-10">

      <h1 className="box pb-10 my-10 mx-10">
          {heroTitle.map((node, index) => {
              return (
              <div>{node}<br/></div>
              )
          })}
      </h1>

<section>
    <BackgroundImage
      Tag="section"
      // Spread bgImage into BackgroundImage:
      {...bgImageFull}
      preserveStackingContext
      className="trigger-animation mx-10">
    <div className="circle--container">
                  <div className="circle--child-trigger"></div>

      <div className="circle--child circle--child-1 ">
      
            <div className="arrows">
                <Arrow />
                <Arrow />
            </div>

      </div>

      <div className="circle--child circle--child-2 m-10">


      </div>
    </div>

    </BackgroundImage>
</section>


    <section className="scrolling-text--container">
      <div className="scrolling-text--wrapper">
        <h2>Super Fast - Super Fast - Super Fast - Super Fast</h2>
        <p>SEO Ready - SEO Ready - SEO Ready - SEO Ready - SEO Ready - SEO Ready</p>
      </div>          
    </section>

    <section className="fire-text--container grid--two pb-6 mx-10">
      <div className="">
        <h2>Why Choose Us?</h2>
      </div>          
      <div className="">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>          
    </section>

    <section >
      <div className="position-relative mb-5">
      <div className="grow--container bg-color--primary"></div>

      <div className="grid--four mx-10">
        <div class="text-center"><h2 className="color--white">One</h2></div>
        <div class="text-center"><h2 className="color--white">Two</h2></div>
        <div class="text-center"><h2 className="color--white">Three</h2></div>
        <div class="text-center"><h2 className="color--white">Four</h2></div>
      </div>
      </div>

      <div className="mx-10 pb-10 pt-5">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>

    </section>

</div>


</LayoutLanding>
  )
}


export const query = graphql`
  {
    wpPage(databaseId: {eq: 239}) {
      id
      title
      page_tolka {
        introText
        featureImage {
          id
          localFile {
            childImageSharp {
              gatsbyImageData
            }
          }
        }  
      }
    }
  }
`


export default TolkaPage