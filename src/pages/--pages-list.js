

import React from "react"
import { graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import Layout from '../components/layout';
import Seo from '../components/seo';

//import NavStatic from '../components/nav-static';


const HomePage = ({data, location}) => 
<Layout>

<Seo location={ location="default"} />

<div className="container">

  <p> List of all the pages on this site.</p>

  <div>
      {data.allWpPage.nodes.map(node=> (
      <div>
          <p>Details = {node.title}</p>
      </div>
      ))}
  </div>

  <p>List of all the posts on this site  </p>

  <div>

  {data.allWpPost.nodes.map(node=> (
    <div>
        <p>title = {node.title}</p>
        <p>slug = {node.slug}</p>
    </div>
  ))}

  </div>

  <p>List of all the custom posts called CITY on this site  </p>

  <div>

  {data.allWpCity.nodes.map(node=> (
    <div>
        <p>title = {node.title}</p>
        <p>slug = {node.slug}</p>
    </div>
  ))}

  </div>


  <h3>Page Content</h3>

  <StaticImage 
        src="../images/building-city.svg" 
        alt="A building"
        placeholder="blurred"
        layout="fixed"
        width={200}
        height={200} />


<div>
<h2>TO DO</h2>
<ul>
  <li>How to get items out of ACF OPTIONS</li>
  <li>menu with sub nav items</li>
  <li>work out best styling options - is theme ui/tailwind the way to do, should I just be doing NO framework?</li>
  <li>Fix warnings - lots of them! ha!</li>
</ul>
</div>


</div>
</Layout>

export const query = graphql`
  {
    allWpPage {
      nodes {
        title
      }
    }
    allWpPost {
      nodes {
        id
        title
        slug
      }
    }
    allWpCity {
      nodes {
        id
        title
        slug
      }
    }
  }
`


export default HomePage