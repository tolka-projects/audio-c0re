import React, {useState} from 'react';

import { graphql } from "gatsby"
// import { FormiumForm } from '@formium/react'
// import { formium } from '../lib/formium'

import Layout from "../components/layout"
import SEO from '../components/seo';


export default function FeedbackForm({ data, location }) {

  const [success, setSuccess] = useState(false);
  if (success) {
    return <div>Thank you! Your response has been recorded.</div>;
  }

  return (
<Layout>

<SEO location={location} />

    <h1> CONTACT US</h1>
      {/* <FormiumForm 
      data={data.formiumForm} 
      onSubmit={async (values) => {
        // Send form values to Formium
        await formium.submitForm('audio-c0re', values)
        setSuccess(true);

      }}/> */}

    </Layout>
  ) 
}

export const query = graphql`
  {
    
    wpMenu(id: {eq: "dGVybTo1MA=="}) {
        menuItems {
          nodes {
            databaseId
            title
            url
            label
          }
        }
      }
    
  }
`


