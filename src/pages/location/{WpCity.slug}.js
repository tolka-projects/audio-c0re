import React from "react"
import { graphql } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { formatDateString } from '../../lib/utils';
import Layout from "../../components/layout"
import Seo from "../../components/seo"

//import FeaturedMedia from "../../components/FeaturedImage"



// export default function Location({ data }) {

// }


const Location = ({ location, 

  data: {
    wpCity: {
      title,
      content,
      dateGmt,
      featuredImage,
    },
  },

 }) => {

  const image = getImage(featuredImage.node.localFile)
  //console.log(image);

  return(
      <Layout>  
       <Seo location={location} />
          <div>
              <h1>{title}</h1>
              <p>{formatDateString(dateGmt)}</p>
    
              <GatsbyImage image={image}  alt=""/>

              <h2>All about {title}</h2>
              <div  dangerouslySetInnerHTML={{ __html: content }} ></div>
          </div>
      </Layout>
  )
}


export const query = graphql`
  query cityPostQuery($id: String!) {
    wpCity(id: { eq: $id }) {
      title
      content
      dateGmt
      featuredImage {
        node {
          altText
          localFile {
            childImageSharp {
              gatsbyImageData(layout: CONSTRAINED, quality: 90)
            }
            publicURL
          }
          mediaDetails {
            width
            height
          }
        }
      }
    }
  }
`;

export default Location