/** @jsx jsx */
import { jsx, Grid, Flex, Link} from "theme-ui"
// import React from "react"
import { graphql } from "gatsby"


const LinkRecipies = ({data}) => {
//console.log(data);



return(


<Flex>
  <Grid gap={2} columns={[ '1fr' , '1fr', '1fr 1fr']}>

    <Flex m={5} sx={{ flexDirection: 'column' }}>

        {data.wpPage.landingPage.ctaButton.map(node=> {
        //console.log(node.ctaButton.target);
         if(node.ctaButton.target === '_blank') {
            return(
              <Flex m={5} sx={{ flexDirection: 'column' }}>
                This is an external link using a straight anchor element. Logic is based on target attribute.
                <p>title = {node.ctaButton.target}</p>
              <a href={node.ctaButton.url} target='_blank' rel="noreferrer" >{node.ctaButton.title}</a>
            </Flex>
            )      
         } else { 
            return (
                <Flex m={5} sx={{ flexDirection: 'column' }}>
                    This is an internal link using a themui link element. Logic is based on target attribute.
                    <p>title = {node.ctaButton.target}</p>
                    <Link href={node.ctaButton.url}>{node.ctaButton.title}</Link>
                </Flex>
            )
            }
      })}

    </Flex>
    
  </Grid>
</Flex>
 
  )
}

export const query = graphql`
  {
    wpPage(databaseId: {eq: 21}) {
      id
      title
      landingPage {
        ctaButton {
          ctaButton {
            target
            title
            url
          }
        }
      }
    }
  }
`


export default LinkRecipies