import { tailwind } from '@theme-ui/presets'

export default {
  ...tailwind,
  colors: {
    ...tailwind.colors, // Gives you the tailwind color presets
    background: "white", // Overrides the default background
    primary: "orange", // Overrides the default primary color
    secondary: "red", // Overrides the default primary color
  },
  links: {
    button: {
      display: 'block',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      padding: '.5rem 1rem',
      borderRadius: '.5rem',
      textDecoration: 'none',
      borderWidth: '1px',
      borderStyle: 'solid',
      transitionDuration: '.2s',
      transitionTimingFunction: 'ease-in-out',
    },
    buttonLight: {
      variant: 'links.button',
      backgroundColor: 'primary',   
      color: 'white', 
      borderColor: 'primary',  
        ':hover' : {
          color: 'primary',
          backgroundColor: 'white',
        },
    },
    buttonDark: {
      variant: 'links.button',
      backgroundColor: 'white',
      color: 'primary',
      borderColor: 'primary',
      ':hover' : {
        color: 'white',
        backgroundColor: 'primary',
      },
    },
     nav: {
      px: 2,
      py: 1,
      textTransform: 'uppercase',
      letterSpacing: '0.2em',
    }
  },
  
  breakpoints: ['40em', '52em', '64em'],
}